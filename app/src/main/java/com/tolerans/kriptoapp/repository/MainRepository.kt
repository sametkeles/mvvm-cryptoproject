package com.tolerans.kriptoapp.repository

import com.tolerans.kriptoapp.domain.ChainDao
import com.tolerans.kriptoapp.util.ChainDatabaseMapper
import com.tolerans.kriptoapp.remote.KriptoAPI
import com.tolerans.kriptoapp.remote.model.ChainEndPointModel
import com.tolerans.kriptoapp.util.DataState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.lang.Exception

class MainRepository constructor(val kriptoAPI: KriptoAPI,val chainDao: ChainDao) {

     fun getChainEndPoint(chainType:String): Flow<DataState<ChainEndPointModel>> = flow {
        emit(DataState.Loading)
        try{
            val networkKripto = kriptoAPI.getChainEndPoint(chainType)
            val endpointChainDatabaseModel = ChainDatabaseMapper.networkChaintoDB(networkKripto)
            chainDao.insertChain(endpointChainDatabaseModel)
            emit(DataState.Success(networkKripto))
        }catch (e:Exception){
            emit(DataState.Error(e))
        }
    }.flowOn(Dispatchers.IO)


}