package com.tolerans.kriptoapp.ui

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.lifecycle.map
import com.github.mikephil.charting.data.Entry
import com.tolerans.kriptoapp.remote.model.ChainEndPointModel
import com.tolerans.kriptoapp.repository.MainRepository
import com.tolerans.kriptoapp.util.DataState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class MainViewModel @ViewModelInject constructor(val repository: MainRepository):ViewModel() {


    val chainList:MutableLiveData<DataState<ArrayList<Entry>>> = MutableLiveData()
    init {
        getDatas()
    }
   fun getDatas(){
       val valueList :ArrayList<Entry> = ArrayList()
       repository.getChainEndPoint("btc").map {
               when(it){
                   is DataState.Success<ChainEndPointModel> -> {
                       //I generated random data and added to the list
                       valueList.add(Entry(0f,it.data.payload.difficulty))
                       valueList.add(Entry(1f, (it.data.payload.difficulty+ 1.0).toFloat()))
                       valueList.add(Entry(2f, (it.data.payload.difficulty+ 1.7).toFloat()))
                       valueList.add(Entry(3f, (it.data.payload.difficulty- 2.0).toFloat()))
                       chainList.postValue(DataState.Success(valueList))
                   }
                   is DataState.Error -> {
                       chainList.postValue(DataState.Error(it.exception))
               }
                   is DataState.Loading -> {
                       chainList.postValue(DataState.Loading)
                   }
               }

       }.flowOn(Dispatchers.IO).launchIn(viewModelScope)

   }

}