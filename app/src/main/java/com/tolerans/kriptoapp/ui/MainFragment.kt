package com.tolerans.kriptoapp.ui

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.tolerans.kriptoapp.R
import com.tolerans.kriptoapp.util.DataState
import com.tolerans.kriptoapp.util.Helper.hideView
import com.tolerans.kriptoapp.util.Helper.showView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class MainFragment : Fragment() {
    private val viewModel:MainViewModel by viewModels()
    lateinit var lineData: LineData
    lateinit var lineDataSet: LineDataSet


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.chainList.observe(viewLifecycleOwner, Observer {
            when(it){
                is DataState.Success<ArrayList<Entry>> ->{
                    hideView(progressBar)
                    hideView(txtException)
                    showView(chartView)
                    lineDataSet = LineDataSet(it.data, "")
                    lineData = LineData(lineDataSet)
                    chartView.data = lineData
                    lineDataSet.setColor(Color.RED)
                    lineDataSet.valueTextColor = Color.BLACK
                    lineDataSet.valueTextSize = 18f
                    chartView.invalidate()
                }
                is DataState.Error ->{
                    Log.e("Main",it.exception.message)
                    txtException.text  = it.exception.message
                    showView(txtException)
                    hideView(progressBar)
                    hideView(chartView)

                }
                is DataState.Loading->{
                    showView(progressBar)
                    hideView(txtException)
                    hideView(chartView)
                }
            }
        })

    }


}