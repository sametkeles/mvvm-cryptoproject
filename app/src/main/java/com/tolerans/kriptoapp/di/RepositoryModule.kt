package com.tolerans.kriptoapp.di

import com.tolerans.kriptoapp.domain.ChainDao
import com.tolerans.kriptoapp.remote.KriptoAPI
import com.tolerans.kriptoapp.repository.MainRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun providesMainRepository(kriptoAPI: KriptoAPI,chainDao: ChainDao):MainRepository{
        return MainRepository(kriptoAPI,chainDao)
    }

}