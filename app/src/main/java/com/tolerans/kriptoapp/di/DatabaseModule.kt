package com.tolerans.kriptoapp.di

import android.content.Context
import androidx.room.Room
import com.tolerans.kriptoapp.domain.ChainDao
import com.tolerans.kriptoapp.domain.ChainDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context):ChainDatabase{
        return Room.databaseBuilder(context,ChainDatabase::class.java,ChainDatabase.DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideDao(chainDatabase: ChainDatabase):ChainDao{
        return chainDatabase.chainDao()
    }

}