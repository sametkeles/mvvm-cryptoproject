package com.tolerans.kriptoapp.di

import android.content.Context
import com.tolerans.kriptoapp.remote.KriptoAPI
import com.tolerans.kriptoapp.util.Constant
import com.tolerans.kriptoapp.util.NetworkConnectionInterceptor
import com.tolerans.kriptoapp.util.TokenInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun provideTokenInterceptor(): TokenInterceptor {
        return TokenInterceptor()
    }
    @Provides
    @Singleton
    fun provideNetworkStateInterceptor(@ApplicationContext context:Context):NetworkConnectionInterceptor{
        return NetworkConnectionInterceptor(context)
    }

    @Provides
    @Singleton
    fun provideClient(tokenInterceptor: TokenInterceptor,networkConnectionInterceptor: NetworkConnectionInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(tokenInterceptor)
            .addInterceptor(networkConnectionInterceptor)
            .build()

    }

    @Provides
    @Singleton
    fun provideRetrofitBuilder(okHttpClient: OkHttpClient):Retrofit.Builder{
        return Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(Constant.baseURL)
    }
    @Provides
    @Singleton
    fun provideService(retrofitBuilder: Retrofit.Builder):KriptoAPI{
        return retrofitBuilder.build().create(KriptoAPI::class.java)
    }
}