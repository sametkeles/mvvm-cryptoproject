package com.tolerans.kriptoapp.util

import android.view.View

object Helper{
    fun hideView(view: View){
        view.visibility = View.GONE
    }
    fun showView(view: View) {
        view.visibility = View.VISIBLE
    }
}