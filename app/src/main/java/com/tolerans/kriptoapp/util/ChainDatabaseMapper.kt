package com.tolerans.kriptoapp.util

import com.tolerans.kriptoapp.domain.model.EndpointChainDatabaseModel
import com.tolerans.kriptoapp.domain.model.Payload
import com.tolerans.kriptoapp.remote.model.ChainEndPointModel

object ChainDatabaseMapper {
    fun networkChaintoDB (endPointModel: ChainEndPointModel):EndpointChainDatabaseModel{
        var payload = Payload(endPointModel.payload.difficulty,endPointModel.payload.headers, endPointModel.payload.chain,endPointModel.payload.chainWork, endPointModel.payload.mediantime,endPointModel.payload.blocks, endPointModel.payload.bestBlockHash,
            endPointModel.payload.currency,endPointModel.payload.transactions,endPointModel.payload.verificationProgress)
        return EndpointChainDatabaseModel(payload = payload)
    }
}