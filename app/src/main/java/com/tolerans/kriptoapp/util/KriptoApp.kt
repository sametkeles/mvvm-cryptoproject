package com.tolerans.kriptoapp.util

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class KriptoApp :Application(){

}