package com.tolerans.kriptoapp.util

import android.content.Context
import android.net.ConnectivityManager
import okhttp3.Interceptor
import okhttp3.Response

class NetworkConnectionInterceptor(val context: Context):Interceptor{
    override fun intercept(chain: Interceptor.Chain): Response {
        if(!isInternetAvaible())
            throw NoInternetException("No Internet Connection")
        return chain.proceed(chain.request())
    }
    private fun isInternetAvaible():Boolean{
        val connectionManager = context.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectionManager.activeNetworkInfo.also {
            return it!=null && it.isConnected
        }
    }

}