package com.tolerans.kriptoapp.util

import java.io.IOException

class NoInternetException(message:String):IOException(message)