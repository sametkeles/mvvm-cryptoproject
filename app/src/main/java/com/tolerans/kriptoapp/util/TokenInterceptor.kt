package com.tolerans.kriptoapp.util

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class TokenInterceptor :Interceptor{
    override fun intercept(chain: Interceptor.Chain): Response {
        var newRequest = chain.request().newBuilder().addHeader("X-API-Key","30b98cfb14c03c42ab88ee3596abf65a18fd8386")
            .addHeader("Content-Type","application/json; charset=utf-8")
            .build();
        return chain.proceed(newRequest)
    }

}