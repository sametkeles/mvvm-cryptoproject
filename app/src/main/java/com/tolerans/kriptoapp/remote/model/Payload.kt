package com.tolerans.kriptoapp.remote.model

data class Payload (
    val difficulty:Float,
    val headers:Int,
    val chain:String,
    val chainWork:String,
    val mediantime:Int,
    val blocks:Int,
    val bestBlockHash:String,
    val currency:String,
    val transactions:Int,
    val verificationProgress:Float,
)
