package com.tolerans.kriptoapp.remote

import com.tolerans.kriptoapp.remote.model.ChainEndPointModel
import com.tolerans.kriptoapp.util.Constant
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface KriptoAPI {

    @GET("{COIN}/{NETWORK}/info")
    suspend fun getChainEndPoint(@Path("COIN") coinType:String,@Path("NETWORK") networkName:String = Constant.networkName):ChainEndPointModel

}