package com.tolerans.kriptoapp.domain

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.tolerans.kriptoapp.domain.converters.PayloadConverter
import com.tolerans.kriptoapp.domain.model.EndpointChainDatabaseModel

@Database(entities = [EndpointChainDatabaseModel::class,],version = 1)
@TypeConverters(PayloadConverter::class)
abstract class ChainDatabase:RoomDatabase() {
    abstract fun chainDao(): ChainDao
    companion object{
        val DATABASE_NAME = "chain_db"
    }

}