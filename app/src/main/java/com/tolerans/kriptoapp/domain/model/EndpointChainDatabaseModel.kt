package com.tolerans.kriptoapp.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.tolerans.kriptoapp.domain.converters.PayloadConverter

@Entity(tableName = "chain_table")
data class EndpointChainDatabaseModel(
    @PrimaryKey(autoGenerate = true)
    val chainID:Int? = 0,
    @ColumnInfo(name ="payload")
    @TypeConverters(PayloadConverter::class)
    var payload: Payload
)