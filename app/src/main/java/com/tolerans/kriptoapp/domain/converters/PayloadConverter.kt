package com.tolerans.kriptoapp.domain.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.tolerans.kriptoapp.domain.model.Payload

class PayloadConverter {

    @TypeConverter
    fun fromPayloadJson(payload: Payload):String{
        return Gson().toJson(payload)
    }

    @TypeConverter
    fun toPayload(json:String):Payload{
        return Gson().fromJson(json,Payload::class.java)
    }

}