package com.tolerans.kriptoapp.domain.model


data class Payload (
    var difficulty:Float,
    var headers:Int,
    var chain:String,
    var chainWork:String,
    var mediantime:Int,
    var blocks:Int,
    var bestBlockHash:String,
    var currency:String,
    var transactions:Int,
    var verificationProgress:Float,
)