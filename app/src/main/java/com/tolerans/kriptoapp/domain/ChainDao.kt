package com.tolerans.kriptoapp.domain

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tolerans.kriptoapp.domain.model.EndpointChainDatabaseModel

@Dao
interface ChainDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertChain(endpointChainDatabaseModel: EndpointChainDatabaseModel):Long

    @Query("Select * from chain_table")
    suspend fun getAllChain():List<EndpointChainDatabaseModel>
}